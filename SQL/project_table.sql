CREATE TABLE `tm_project` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`created` TIMESTAMP NULL DEFAULT NULL,
	`dateStart` TIMESTAMP NULL DEFAULT NULL,
	`dateFinish` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK_tm_project_tm_user` (`userId`) USING BTREE,
	CONSTRAINT `FK_tm_project_tm_user` FOREIGN KEY (`userId`) REFERENCES `task-manager`.`tm_user` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
;
