package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity{

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstname;

    @Nullable
    private String lastname;

    @Nullable
    private String middlename;

    @NotNull
    private Role role = Role.USER;

    private boolean locked = false;

}
