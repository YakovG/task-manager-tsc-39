package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface ITaskEndpoint {
    @WebMethod
    @SneakyThrows
    void addTaskAll(
            @WebParam(name = "session") Session session,
            @WebParam(name = "collection") Collection<Task> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task addTask(
            @WebParam(name = "session") Session session,
            @WebParam(name = "task") Task model
    );

    @Nullable Task addTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<Task> findTaskAll(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<Task> findTaskAllByUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    int sizeTask(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void removeTask(
            @WebParam(name = "session") Session session,
            @WebParam(name = "task") Task model
    );

    @WebMethod
    @SneakyThrows
    void clearTask(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task removeTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task removeTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task removeTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task startTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task startTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task startTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task finishTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task finishTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task finishTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<Task> sortedTaskBy(
            @WebParam(name = "session") Session session,
            @WebParam(name = "sortCheck") String sortCheck);

    @WebMethod
    @SneakyThrows
    @Nullable Task updateTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String modelId,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task updateTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task changeTaskStatusById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String id,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task changeTaskStatusByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task changeTaskStatusByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    void checkTaskAccess(
            @WebParam(name = "session") Session session
    );
}
