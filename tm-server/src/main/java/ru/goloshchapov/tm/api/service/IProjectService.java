package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IBusinessService<Project> {

    @Nullable
    @Override
    @SneakyThrows
    Project add(@Nullable Project project);

    @Nullable
    @SneakyThrows
    Project add(@Nullable String userId, @Nullable Project project);

    @Nullable
    Project add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @SneakyThrows
    void addAll(@Nullable String userId, @Nullable Collection<Project> collection);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project project);

    @Nullable
    @SneakyThrows
    List<Project> findAllByUserId(@Nullable String userId);

    @Nullable
    @Override
    @SneakyThrows
    List<Project> findAll();

    @Nullable
    @SneakyThrows
    List<Project> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Project findOneById(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String projectId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String projectName);

    @Nullable
    @SneakyThrows
    Project findOneByName(@Nullable String name);

    @Nullable
    @SneakyThrows
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @SneakyThrows
    Project removeOneById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    @SneakyThrows
    Project removeOneById(@Nullable String projectId);

    @SneakyThrows
    void clear(@Nullable String userId);
}
