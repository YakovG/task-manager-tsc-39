package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Domain;

public interface IDataService {

    @Nullable Domain getDomain();

    void setDomain(@Nullable Domain domain);

    void loadBase64Data();

    void saveBase64Data();

    void saveBinaryData();

    void loadBinaryData();

    void loadDataJsonFasterXml();

    void saveDataJsonFasterXml();

    void saveJsonJaxBData();

    void loadJsonJaxBData();

    void saveXmlFasterXmlData();

    void loadXmlFasterXmlData();

    void saveXmlJaxBData();

    void loadXmlJaxBData();

    void saveYamlData();

    void loadYamlData();

    void saveBackup();

    void loadBackup();

    void clearBackup();
}
