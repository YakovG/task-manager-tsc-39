package ru.goloshchapov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface ITestService {

    @NotNull SqlSession getSqlSession();

    SqlSessionFactory getSqlSessionFactory();

    Connection getConnection();

    void initTestUserTable();

    void initTestSessionTable();

    void initTestProjectTable();

    void initTestTaskTable();

    void dropDatabase();
}
