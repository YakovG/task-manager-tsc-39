package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface IAdminEndpoint {
    @WebMethod
    @SneakyThrows
    void addUserAll(
            @WebParam(name = "session") Session session,
            @WebParam(name = "collection") Collection<User> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable User addUser(
            @WebParam(name = "session") Session session,
            @WebParam(name = "user") User entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<User> findUserAll(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable User findUserById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "userId") String id);

    @WebMethod
    @SneakyThrows
    @Nullable User findUserByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index);

    @WebMethod
    @SneakyThrows
    boolean isUserAbsentById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "userId") String id);

    @WebMethod
    @SneakyThrows
    boolean isUserAbsentByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index);

    @WebMethod
    @SneakyThrows
    String getUserIdByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index);

    void showAllUserWithProject(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void showUserList(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    int sizeUser(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void clearUser(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable User removeUserById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "userId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable User removeUserByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isUserLoginExists(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    boolean isEmailExists(
            @WebParam(name = "session") Session session,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull User createUser(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @SneakyThrows
    @NotNull User createUserWithEmail(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull User createUserWithRole(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "role") Role role
    );

    @WebMethod
    @SneakyThrows
    @NotNull User createUserWithAll(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email,
            @WebParam(name = "role") String role
    );

    @WebMethod
    @SneakyThrows
    @NotNull User findUserByLogin(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login
    );

    @WebMethod
    @SneakyThrows
    @NotNull User findUserByEmail(
            @WebParam(name = "session") Session session,
            @WebParam(name = "email") String email);

    @WebMethod
    @SneakyThrows
    @Nullable User removeUser(
            @WebParam(name = "session") Session session,
            @WebParam(name = "user") User user);

    @WebMethod
    @SneakyThrows
    @NotNull User removeUserByLogin(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    @NotNull User removeUserByEmail(
            @WebParam(name = "session") Session session,
            @WebParam(name = "email") String email);

    @WebMethod
    @SneakyThrows
    @NotNull User lockUserByLogin(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login
    );

    @WebMethod
    @SneakyThrows
    @NotNull User unlockUserByLogin(
            @WebParam(name = "session") Session session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    @NotNull User lockUserByEmail(
            @WebParam(name = "session") Session session,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull User unlockUserByEmail(
            @WebParam(name = "session") Session session,
            @WebParam(name = "email") String email
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    String getProjectIdByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name ") String name
    );

    @WebMethod
    @SneakyThrows
    void addProjectAllWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "collection") Collection<Project> collection
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project addProjectWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "project") Project entity
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Project> findProjectAllWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String id
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String id
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    String getProjectIdByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    int sizeProjectWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void clearProjectWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void removeProjectWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "project") Project entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable Project removeProjectByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable Project removeProjectByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    String getTaskIdByNameWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    void addTaskAllWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "collection") Collection<Task> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task addTaskWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "task") Task entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<Task> findTaskAllWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task findTaskByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    String getTaskIdByIndexWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    int sizeTaskWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void clearTaskWithoutUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void removeTaskWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "task") Task entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable Task removeTaskByIdWithoutUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveBase64Data(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadBase64Data(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveBinaryData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadBinaryData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveJsonFasterXmlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadJsonFasterXmlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveJsonJaxBData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadJsonJaxBData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveXmlFasterXmlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadXmlFasterXmlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveXmlJaxBData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadXmlJaxBData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveYamlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadYamlData(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result saveBackup(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result loadBackup(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @SneakyThrows
    @NotNull Result clearBackup(
            @WebParam(name = "session") @Nullable Session session
    );
}
