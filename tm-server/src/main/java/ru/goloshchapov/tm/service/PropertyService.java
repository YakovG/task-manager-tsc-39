package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String VALUE_DEFAULT_STRING = "";

    @NotNull
    private static final String VALUE_DEFAULT_INTEGER = "0";

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY_JAVA_OPTS = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_KEY_ENVIRONMENT = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY_JAVA_OPTS = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY_ENVIRONMENT = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final Integer PASSWORD_ITERATION_DEFAULT = 1;

    @NotNull
    private static final String APPLICATION_VERSION_KEY_JAVA_OPTS = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_KEY_ENVIRONMENT = "APPLICATION_VERSION";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String SERVER_HOST_JAVA_OPTS = "server.host";

    @NotNull
    private static final String SERVER_HOST_ENVIRONMENT = "SERVER_HOST";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_PORT_JAVA_OPTS = "server.port";

    @NotNull
    private static final String SERVER_PORT_ENVIRONMENT = "SERVER_PORT";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SESSION_SALT_JAVA_OPTS = "session.salt";

    @NotNull
    private static final String SESSION_SALT_ENVIRONMENT = "SESSION_SALT";

    @NotNull
    private static final String SESSION_SALT = "session.salt";

    @NotNull
    private static final String SESSION_SALT_DEFAULT = "";

    @NotNull
    private static final String SESSION_CYCLE_JAVA_OPTS = "session.cycle";

    @NotNull
    private static final String SESSION_CYCLE_ENVIRONMENT = "SESSION_CYCLE";

    @NotNull
    private static final String SESSION_CYCLE = "session.cycle";

    @NotNull
    private static final Integer SESSION_CYCLE_DEFAULT = 1;

    @NotNull
    private static final String JDBC_USERNAME_JAVA_OPTS = "jdbc.username";

    @NotNull
    private static final String JDBC_USERNAME_ENVIRONMENT = "JDBC_USERNAME";

    @NotNull
    private static final String JDBC_USERNAME = "jdbc.username";

    @NotNull
    private static final String JDBC_USERNAME_DEFAULT = "";

    @NotNull
    private static final String JDBC_PASSWORD_JAVA_OPTS = "jdbc.password";

    @NotNull
    private static final String JDBC_PASSWORD_ENVIRONMENT = "JDBC_PASSWORD";

    @NotNull
    private static final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT = "";

    @NotNull
    private static final String JDBC_URL_JAVA_OPTS = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_ENVIRONMENT = "JDBC_URL";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_DEFAULT = "";

    @NotNull
    private static final String JDBC_DRIVER_JAVA_OPTS = "jdbc.driver";

    @NotNull
    private static final String JDBC_DRIVER_ENVIRONMENT = "JDBC_DRIVER";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_DRIVER_DEFAULT = "";

    @NotNull
    private static final String JDBC_TEST_USERNAME_JAVA_OPTS = "jdbctest.username";

    @NotNull
    private static final String JDBC_TEST_USERNAME_ENVIRONMENT = "JDBCTEST_USERNAME";

    @NotNull
    private static final String JDBC_TEST_USERNAME = "jdbctest.username";

    @NotNull
    private static final String JDBC_TEST_USERNAME_DEFAULT = "";

    @NotNull
    private static final String JDBC_TEST_PASSWORD_JAVA_OPTS = "jdbctest.password";

    @NotNull
    private static final String JDBC_TEST_PASSWORD_ENVIRONMENT = "JDBCTEST_PASSWORD";

    @NotNull
    private static final String JDBC_TEST_PASSWORD = "jdbctest.password";

    @NotNull
    private static final String JDBC_TEST_PASSWORD_DEFAULT = "";

    @NotNull
    private static final String JDBC_TEST_URL_JAVA_OPTS = "jdbctest.url";

    @NotNull
    private static final String JDBC_TEST_URL_ENVIRONMENT = "JDBCTEST_URL";

    @NotNull
    private static final String JDBC_TEST_URL = "jdbctest.url";

    @NotNull
    private static final String JDBC_TEST_URL_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    public String getValue(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final String keyValue,
            @Nullable final String defaultValue
    ) {
        return getValueString(javaOpts, environment, keyValue, defaultValue);
    }

    @NotNull
    public String getValueString(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final String keyValue,
            @Nullable final String defaultValue
    ) {
        if (isEmpty(javaOpts)) return VALUE_DEFAULT_STRING;
        if (isEmpty(environment)) return VALUE_DEFAULT_STRING;
        if (isEmpty(keyValue)) return VALUE_DEFAULT_STRING;
        if (defaultValue == null) return VALUE_DEFAULT_STRING;
        if (System.getProperties().containsKey(javaOpts))
            return System.getProperty(javaOpts);
        if (System.getenv().containsKey(environment))
            return System.getenv(environment);
        return properties.getProperty(keyValue, defaultValue);
    }

    @NotNull
    public Integer getValueInteger(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final String keyValue,
            @Nullable final Integer defaultValue
    ) {
        @NotNull final String value = getValue(javaOpts, environment, keyValue, VALUE_DEFAULT_INTEGER);
        if (isEmpty(value))
            if (defaultValue == null) return Integer.parseInt(VALUE_DEFAULT_INTEGER);
        return Integer.parseInt(value);

    }


    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(
                PASSWORD_SECRET_KEY_JAVA_OPTS,
                PASSWORD_SECRET_KEY_ENVIRONMENT,
                PASSWORD_SECRET_KEY,
                PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInteger(
                PASSWORD_ITERATION_KEY_JAVA_OPTS,
                PASSWORD_ITERATION_KEY_ENVIRONMENT,
                PASSWORD_ITERATION_KEY,
                PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
       return getValueString(
               APPLICATION_VERSION_KEY_JAVA_OPTS,
               APPLICATION_VERSION_KEY_ENVIRONMENT,
               APPLICATION_VERSION_KEY,
               APPLICATION_VERSION_DEFAULT
       );
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getValueString(
                SERVER_HOST_JAVA_OPTS,
                SERVER_HOST_ENVIRONMENT,
                SERVER_HOST,
                SERVER_HOST_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getValueString(
                SERVER_PORT_JAVA_OPTS,
                SERVER_PORT_ENVIRONMENT,
                SERVER_PORT,
                SERVER_PORT_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return getValueString(
                SESSION_SALT_JAVA_OPTS,
                SESSION_SALT_ENVIRONMENT,
                SESSION_SALT,
                SESSION_SALT_DEFAULT
        );
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return getValueInteger(
                SESSION_CYCLE_JAVA_OPTS,
                SESSION_CYCLE_ENVIRONMENT,
                SESSION_CYCLE,
                SESSION_CYCLE_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        return getValueString(
                JDBC_USERNAME_JAVA_OPTS,
                JDBC_USERNAME_ENVIRONMENT,
                JDBC_USERNAME,
                JDBC_USERNAME_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getValueString(
                JDBC_PASSWORD_JAVA_OPTS,
                JDBC_PASSWORD_ENVIRONMENT,
                JDBC_PASSWORD,
                JDBC_PASSWORD_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getValueString(
                JDBC_URL_JAVA_OPTS,
                JDBC_URL_ENVIRONMENT,
                JDBC_URL,
                JDBC_URL_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return getValueString(
                JDBC_DRIVER_JAVA_OPTS,
                JDBC_DRIVER_ENVIRONMENT,
                JDBC_DRIVER,
                JDBC_DRIVER_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcTestUsername() {
        return getValueString(
                JDBC_TEST_USERNAME_JAVA_OPTS,
                JDBC_TEST_USERNAME_ENVIRONMENT,
                JDBC_TEST_USERNAME,
                JDBC_TEST_USERNAME_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getJdbcTestPassword() {
        return getValueString(
                JDBC_TEST_PASSWORD_JAVA_OPTS,
                JDBC_TEST_PASSWORD_ENVIRONMENT,
                JDBC_TEST_PASSWORD,
                JDBC_TEST_PASSWORD_DEFAULT
        );
    }


    @NotNull
    @Override
    public String getJdbcTestUrl() {
        return getValueString(
                JDBC_TEST_URL_JAVA_OPTS,
                JDBC_TEST_URL_ENVIRONMENT,
                JDBC_TEST_URL,
                JDBC_TEST_URL_DEFAULT
        );
    }
}
