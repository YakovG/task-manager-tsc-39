package ru.goloshchapov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.IDataService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataService implements IDataService {

    @NotNull private static final String FILE_BINARY = "./data.bin";
    @NotNull private static final String FILE_BASE64 = "./data.base64";

    @NotNull private static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";
    @NotNull private static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";
    @NotNull private static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull private static final String FILE_JAXB_JSON = "./data-jaxb.json";
    @NotNull private static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull private static final String BACKUP_XML = "./backup.xml";

    @NotNull private static final String SYSTEM_PROPERTY_KEY = "javax.xml.bind.context.factory";

    @NotNull private static final String SYSTEM_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull private static final String UNMARSHALLER_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull private static final String UNMARSHALLER_PROPERTY_VALUE = "application/json";

    @Nullable private final ServiceLocator serviceLocator;

    public DataService (@Nullable final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @Override
    @SneakyThrows
    public void saveBase64Data() {
        @Nullable final Domain domain = getDomain();

        @NotNull
        final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBase64Data() {
        @Nullable final String base64data = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveBinaryData() {
        @Nullable final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBinaryData() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXml() {
        @Nullable final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXml() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveJsonJaxBData() {
        System.setProperty(SYSTEM_PROPERTY_KEY,SYSTEM_PROPERTY_VALUE);
        @Nullable final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(UNMARSHALLER_PROPERTY_NAME, UNMARSHALLER_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadJsonJaxBData() {
        System.setProperty(SYSTEM_PROPERTY_KEY,SYSTEM_PROPERTY_VALUE);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UNMARSHALLER_PROPERTY_NAME, UNMARSHALLER_PROPERTY_VALUE);
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveXmlFasterXmlData() {
        @Nullable final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadXmlFasterXmlData() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final XmlMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveXmlJaxBData() {
        @Nullable final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_XML);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadXmlJaxBData() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveYamlData() {
        @Nullable final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadYamlData() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveBackup() {
        @Nullable final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBackup() {
        @NotNull final File file = new File(BACKUP_XML);
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final XmlMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void clearBackup() {
        @NotNull final File file = new File(BACKUP_XML);
        if (file.exists()) file.delete();
    }
}
