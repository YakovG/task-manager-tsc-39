package ru.goloshchapov.tm.endpoint;

import ru.goloshchapov.tm.api.endpoint.ICalculatorEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class CalculatorEndpoint extends AbstractEndpoint implements ICalculatorEndpoint {

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

}
