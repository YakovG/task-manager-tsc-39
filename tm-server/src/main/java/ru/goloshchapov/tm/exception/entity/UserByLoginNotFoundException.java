package ru.goloshchapov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class UserByLoginNotFoundException extends AbstractException {

    public UserByLoginNotFoundException(@Nullable final String message) {
        super("Error! User with LOGIN: " + message + " not found");
    }

}
