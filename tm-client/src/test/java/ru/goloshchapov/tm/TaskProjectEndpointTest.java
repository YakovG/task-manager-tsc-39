package ru.goloshchapov.tm;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.*;

import java.util.List;

public class TaskProjectEndpointTest {

    private static final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    private static final TaskProjectEndpointService taskProjectEndpointService =
            new TaskProjectEndpointService();

    private static final TaskProjectEndpoint taskProjectEndpoint =
            taskProjectEndpointService.getTaskProjectEndpointPort();

    private static Session session;

    @BeforeClass
    public static void before() {
        session = sessionEndpoint.openSession("test", "test");
    }

    @AfterClass
    public static void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testFind() {
        Assert.assertNotNull(taskProjectEndpoint.findAllTaskByProjectId(session, "ppp222"));
        Assert.assertFalse(taskProjectEndpoint.isEmptyProjectWithTaskById(session,"ppp222"));
        Assert.assertNotNull(taskProjectEndpoint.findAllTaskByProjectName(session, "Project_3"));
        Assert.assertFalse(taskProjectEndpoint.isEmptyProjectWithTaskByName(session, "Project_3"));
    }

    @Test
    public void testBindUnbind() {
        final List<Task> tasks = taskProjectEndpoint.findAllTaskByProjectId(session,"ppp222");
        Assert.assertFalse(tasks.isEmpty());
        final Task task = tasks.get(0);
        Assert.assertNotNull(task);
        final String id = task.getId();
        final String name = task.getName();
        final String description = task.getDescription();
        final Status status = task.getStatus();
        Task task1 = taskProjectEndpoint.unbindTaskFromProjectById(session, id, "ppp222");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals(name, task1.getName());
        Assert.assertEquals(description, task1.getDescription());
        Assert.assertEquals(status, task1.getStatus());
        Assert.assertNull(task1.getProjectId());
        task1 = taskProjectEndpoint.bindTaskToProjectById(session, id, "ppp222");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals(name, task1.getName());
        Assert.assertEquals(description, task1.getDescription());
        Assert.assertEquals(status, task1.getStatus());
        Assert.assertNotNull(task1.getProjectId());
        Assert.assertEquals("ppp222", task1.getProjectId());
    }

    @Test
    public void testRemove() {
        Assert.assertFalse(taskProjectEndpoint.findAllTaskByProjectId(session,"ppp222").isEmpty());
        Assert.assertFalse(taskProjectEndpoint.findAllTaskByProjectName(session,"Project_3").isEmpty());
        final Project project = taskProjectEndpoint.removeProjectByIdWithTask(session, "ppp222");
        Assert.assertNotNull(project);
        Assert.assertEquals("ppp222", project.getId());
        Assert.assertTrue(taskProjectEndpoint.findAllTaskByProjectId(session,"ppp222").isEmpty());
        Assert.assertFalse(taskProjectEndpoint.removeAllTaskByProjectName(session,"Project_3").isEmpty());
        Assert.assertTrue(taskProjectEndpoint.findAllTaskByProjectName(session,"Project_3").isEmpty());
    }
}
