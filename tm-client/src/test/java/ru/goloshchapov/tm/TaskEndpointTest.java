package ru.goloshchapov.tm;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.*;

public class TaskEndpointTest {

    private static final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    private static final TaskEndpointService taskEndpointService =
            new TaskEndpointService();

    private static final TaskEndpoint taskEndpoint =
            taskEndpointService.getTaskEndpointPort();

    private static Session session;

    @BeforeClass
    public static void before() {
        session = sessionEndpoint.openSession("test", "test");
    }

    @AfterClass
    public static void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testFindAllTask() {
        Assert.assertFalse(taskEndpoint.findTaskAll(session).isEmpty());
        Assert.assertFalse(taskEndpoint.findTaskAllByUserId(session).isEmpty());
        Assert.assertEquals(4, taskEndpoint.sizeTask(session));
        Assert.assertFalse(taskEndpoint.sortedTaskBy(session, "name").isEmpty());
    }

    @Test
    public void testAddFindRemoveTask() {
        final Task task = taskEndpoint.addTaskByName(session,"task", "description");
        Assert.assertNotNull(task);
        final String id = task.getId();
        Assert.assertEquals("task", task.getName());
        Assert.assertEquals("description", task.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        Task task1 = taskEndpoint.findTaskByName(session, "task");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        task1 = taskEndpoint.findTaskById(session, id);
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        taskEndpoint.removeTaskById(session, id);
        Assert.assertNotNull(task);
        Assert.assertNull(taskEndpoint.findTaskById(session,id));
    }

    @Test
    public void testStartFinishTask() {
        final Task task = taskEndpoint.addTaskByName(session,"task", "description");
        Assert.assertNotNull(task);
        final String id = task.getId();
        Assert.assertEquals("task", task.getName());
        Assert.assertEquals("description", task.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        Task task1 = taskEndpoint.startTaskById(session, id);
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
        task1 = taskEndpoint.startTaskByName(session, "task");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
        task1 = taskEndpoint.finishTaskById(session, id);
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.COMPLETE, task1.getStatus());
        task1 = taskEndpoint.finishTaskByName(session, "task");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.COMPLETE, task1.getStatus());
        taskEndpoint.removeTaskByName(session, "task");
        Assert.assertNotNull(task);
        Assert.assertNull(taskEndpoint.findTaskById(session,id));
    }

    @Test
    public void testChangeTaskStatus() {
        final Task task = taskEndpoint.addTaskByName(session,"task", "description");
        Assert.assertNotNull(task);
        final String id = task.getId();
        Assert.assertEquals("task", task.getName());
        Assert.assertEquals("description", task.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        Task task1 = taskEndpoint.changeTaskStatusById(session, id, "IN_PROGRESS");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
        task1 = taskEndpoint.changeTaskStatusByName(session, "task", "COMPLETE");
        Assert.assertNotNull(task1);
        Assert.assertEquals(id, task1.getId());
        Assert.assertEquals("task", task1.getName());
        Assert.assertEquals("description", task1.getDescription());
        Assert.assertEquals(Status.COMPLETE, task1.getStatus());
        taskEndpoint.removeTaskByName(session, "task");
        Assert.assertNotNull(task);
        Assert.assertNull(taskEndpoint.findTaskById(session,id));
    }

    @Test
    public void testUpdateTask() {
        final Task task = taskEndpoint.addTaskByName(session,"task", "description");
        Assert.assertNotNull(task);
        final String id = task.getId();
        Assert.assertEquals("task", task.getName());
        Assert.assertEquals("description", task.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        Assert.assertNotNull(taskEndpoint.updateTaskById(session, id, "title", "about"));
        Assert.assertNotNull(taskEndpoint.findTaskById(session, id));
        Assert.assertNull(taskEndpoint.findTaskByName(session, "task"));
        final Task task1 = taskEndpoint.findTaskByName(session, "title");
        Assert.assertNotNull(task1);
        Assert.assertEquals("about", task1.getDescription());
        taskEndpoint.removeTaskById(session, id);
        Assert.assertNotNull(task);
        Assert.assertNull(taskEndpoint.findTaskById(session,id));
    }

}
