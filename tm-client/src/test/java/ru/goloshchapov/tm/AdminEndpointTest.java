package ru.goloshchapov.tm;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.*;
import ru.goloshchapov.tm.service.PropertyService;

import static ru.goloshchapov.tm.constant.ResultConst.RESULT_SUCCESS;
import static ru.goloshchapov.tm.util.HashUtil.salt;

public class AdminEndpointTest {

    private static final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    private static final AdminEndpointService adminEndpointService =
            new AdminEndpointService();

    private static final AdminEndpoint adminEndpoint =
            adminEndpointService.getAdminEndpointPort();

    private static Session session;

    private static final PropertyService propertyService = new PropertyService();

    @BeforeClass
    public static void before() {
        session = sessionEndpoint.openSession("root", "root");
    }

    @AfterClass
    public static void after() {
        adminEndpoint.clearUser(session);
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testCreateRemoveUser() {
        final User user = adminEndpoint.createUser(session, "user", "user");
        Assert.assertNotNull(user);
        Assert.assertEquals("user", user.getLogin());
        final String password = salt(propertyService, "user");
        Assert.assertEquals(password, user.getPasswordHash());
        final User userRemoved = adminEndpoint.removeUser(session, user);
        Assert.assertNotNull(userRemoved);
        Assert.assertEquals("user", userRemoved.getLogin());
        Assert.assertEquals(password, userRemoved.getPasswordHash());
    }

    @Test
    public void testFindUser() {
        final User user = adminEndpoint.createUserWithEmail(session, "user", "user", "user@user");
        Assert.assertNotNull(user);
        Assert.assertEquals("user", user.getLogin());
        Assert.assertEquals("user@user", user.getEmail());
        final String password = salt(propertyService, "user");
        Assert.assertEquals(password, user.getPasswordHash());
        Assert.assertFalse(adminEndpoint.findUserAll(session).isEmpty());
        User user1 = adminEndpoint.findUserByLogin(session, "user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user", user1.getLogin());
        Assert.assertEquals("user@user", user1.getEmail());
        Assert.assertEquals(password, user1.getPasswordHash());
        user1 = adminEndpoint.findUserByEmail(session, "user@user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user", user1.getLogin());
        Assert.assertEquals("user@user", user.getEmail());
        Assert.assertEquals(password, user1.getPasswordHash());
        user1 = adminEndpoint.findUserByIndex(session, 0);
        Assert.assertNotNull(user1);
        Assert.assertEquals("user", user1.getLogin());
        Assert.assertEquals("user@user", user.getEmail());
        Assert.assertEquals(password, user1.getPasswordHash());
    }

    @Test
    public void testCreateUserWithAll() {
        final User user =
                adminEndpoint.createUserWithAll(session, "user", "user", "user@user", "USER");
        Assert.assertNotNull(user);
        Assert.assertEquals("user", user.getLogin());
        Assert.assertEquals("user@user", user.getEmail());
        final String password = salt(propertyService, "user");
        Assert.assertEquals(password, user.getPasswordHash());
        Assert.assertEquals("USER", user.getRole().value());
        Assert.assertTrue(adminEndpoint.isUserLoginExists(session,"user"));
        Assert.assertTrue(adminEndpoint.isEmailExists(session, "user@user"));
        Assert.assertEquals(1, adminEndpoint.sizeUser(session));
    }

    @Test
    public void testLockUnlockUser() {
        final User user = adminEndpoint.createUserWithEmail(session, "user", "user", "user@user");
        Assert.assertNotNull(user);
        Assert.assertEquals("user", user.getLogin());
        Assert.assertEquals("user@user", user.getEmail());
        final String password = salt(propertyService, "user");
        Assert.assertEquals(password, user.getPasswordHash());
        Assert.assertFalse(adminEndpoint.findUserAll(session).isEmpty());
        Assert.assertFalse(user.isLocked());
        User user1 = adminEndpoint.lockUserByLogin(session, "user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user", user1.getLogin());
        Assert.assertTrue(user1.isLocked());
        user1 = adminEndpoint.unlockUserByLogin(session, "user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user", user1.getLogin());
        Assert.assertFalse(user1.isLocked());
        user1 = adminEndpoint.lockUserByEmail(session, "user@user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user@user", user1.getEmail());
        Assert.assertTrue(user1.isLocked());
        user1 = adminEndpoint.unlockUserByEmail(session, "user@user");
        Assert.assertNotNull(user1);
        Assert.assertEquals("user@user", user1.getEmail());
        Assert.assertFalse(user1.isLocked());
    }

    @Test
    public void testBase64() {
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveBase64Data(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadBase64Data(session).getResult());
    }

    @Test
    public void testBinary() {
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveBinaryData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadBinaryData(session).getResult());
    }

    @Test
    public void testYaml() {
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveYamlData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadYamlData(session).getResult());
    }

    @Test
    public void testJson() {
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveJsonJaxBData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadJsonJaxBData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveJsonFasterXmlData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadJsonFasterXmlData(session).getResult());
    }

    @Test
    public void testXml() {
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveXmlJaxBData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadXmlJaxBData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.saveXmlFasterXmlData(session).getResult());
        Assert.assertEquals(RESULT_SUCCESS, adminEndpoint.loadXmlFasterXmlData(session).getResult());
    }
}
