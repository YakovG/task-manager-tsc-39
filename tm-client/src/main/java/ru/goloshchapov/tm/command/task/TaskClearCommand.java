package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;

public final class TaskClearCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-clear";

    @NotNull public static final String DESCRIPTION = "Clear all tasks";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[TASK CLEAR]");
        endpointLocator.getTaskEndpoint().clearTask(session);
    }
}
